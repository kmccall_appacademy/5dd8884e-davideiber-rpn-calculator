class RPNCalculator

  def initialize
		@stack = []
	end

	def push(num)
		@stack << num.to_f
	end

	def plus
		perform_op(:+)
	end

	def minus
		perform_op(:-)
	end

	def times
		perform_op(:*)
	end

	def divide
		perform_op(:/)
	end

	def value
		@stack.last
	end

	def tokens(string)
		string.split.map do |op|
			case op
			when "+"
				:+
			when "-"
				:-
			when "*"
				:*
			when "/"
				:/
			else
				op.to_f
			end
		end
	end

	def evaluate(string)
		tokens(string).each do |operator|
			case operator
			when :+
				plus
			when :-
				minus
			when :*
				times
			when :/
				divide
			else
				push operator
			end
		end
		value
	end

#
	private

	def perform_op(operation)
		raise "calculator is empty" if @stack.size < 2
		b = @stack.pop
		a = @stack.pop
		case operation
		when :+
			result = a + b
		when :-
			result = a - b
		when :*
			result = a * b
		when :/
			result = a / b
		end
		@stack << result
	end
end
#
